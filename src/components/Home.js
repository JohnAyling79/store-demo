import { connect } from 'react-redux';
import { addToCart } from '../redux/actions/cartActions';

const Home = (props) => {
	const handleClick = (id) => {
		props.addToCart(id);
	}

	const itemList = props.items.map(item => {
		return (
			<div className="col s12 m6 l4" key={item.id}>
				<div className="card">
					<div className="card-image">
						<img src={item.img} alt={item.title} />
						<span className="card-title black-text">{item.title}</span>
						<span to="/" className="btn-floating halfway-fab waves-effect waves-light red">
							<i className="material-icons" onClick={() => { handleClick(item.id) }}>add</i>
						</span>
					</div>
					<div className="card-content">
						<p>{item.desc}</p>
						<p><b>Price: ${item.price}.00</b></p>
					</div>
				</div>
			</div>
		);
	})

	return (
		<div className="container">
			<h3 className="center">Our items</h3>
			<div className="row">
				{itemList}
			</div>
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		items: Object.values(state.items)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		addToCart: (id) => { dispatch(addToCart(id)) }
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);