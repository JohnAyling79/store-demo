import { useEffect } from 'react';
import { connect } from 'react-redux'
import { addShipping, subtractShipping } from '../redux/actions/cartActions';

const Recipe = (props) => {
    useEffect(() => {
        if (props.addShippingToTotal && props.items.length === 0) {
            props.subtractShipping();
        }
    });

    const handleChange = () => {
        if (props.addShippingToTotal) {
            props.subtractShipping();
        } else {
            props.addShipping();
        }
    }

    let total = props.items.reduce((sum, item) => {
        return sum + (item.price * item.quantity);
    }, 0);

    if (props.addShippingToTotal) {
        total += 6;
    }

    return (
        <div className="container">
            <div className="collection">
                <li className="collection-item">
                    <label>
                        <input type="checkbox" checked={props.addShippingToTotal} onChange={handleChange} disabled={props.items.length === 0}/>
                        <span>Shipping(+$6)</span>
                    </label>
                </li>
                <li className="collection-item"><b>Total: ${total}.00</b></li>
            </div>
            <div className="checkout">
                <button className="waves-effect waves-light btn" disabled={props.items.length === 0}>Checkout</button>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        addShippingToTotal: state.addShipping,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addShipping: () => { dispatch(addShipping()) },
        subtractShipping: () => { dispatch(subtractShipping()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Recipe)