import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

const Navbar = (props) => {
  const itemCount = props.items.reduce((total, item) => {
    return total + item.quantity
  }, 0);


  return (
    <nav className="nav-wrapper">
      <div className="container">
        <Link to="/" className="brand-logo left">Shopping</Link>
        <ul className="right">
          <li><Link to="/">Shop</Link></li>
          <li><Link to="/cart">My cart{itemCount ? <span className="badge" data-badge-caption="items">{itemCount}</span> : ''}</Link></li>
        </ul>
      </div>
    </nav>
  );
}

const mapStateToProps = (state) => {
  return {
    items: Object.values(state.addedItems),
  }
}

export default connect(mapStateToProps)(Navbar);