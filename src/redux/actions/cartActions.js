import {
	ADD_TO_CART,
	ADD_TO_QUANTITY,
	SUBTRACT_FROM_QUANTITY,
	REMOVE_ITEM,
	ADD_SHIPPING,
	SUBTRACT_SHIPPING,
 } from '../constants';

export const addToCart = (id) => {
	return {
		type: ADD_TO_CART,
		id
	}
}

export const addQuantity = (id) => {
	return {
		type: ADD_TO_QUANTITY,
		id
	}
}

export const subtractQuantity = (id) => {
	return {
		type: SUBTRACT_FROM_QUANTITY,
		id
	}
}

export const removeItem = (id) => {
	return {
		type: REMOVE_ITEM,
		id
	}
}

export const addShipping = () => {
	return {
		type: ADD_SHIPPING,
	}
}

export const subtractShipping = () => {
	return {
		type: SUBTRACT_SHIPPING,
	}
}