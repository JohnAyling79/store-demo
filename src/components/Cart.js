import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { removeItem, addQuantity, subtractQuantity } from '../redux/actions/cartActions';
import Recipe from './Recipe';

const Cart = (props) => {
    const handlerRemove = (id) => {
        props.removeItem(id);
    }

    const handlerAddQuantity = (id) => {
        props.addQuantity(id);
    }

    const handlerSubtractQuantity = (id) => {
        props.subtractQuantity(id);
    }

    let addedItems = <p style={{ marginLeft: 200 }}>Nothing.</p>;

    if (props.items.length > 0) {
        addedItems = props.items.map(item => (
            <li className="collection-item avatar" key={item.id}>
                <img src={item.img} alt={item.img} className="responsive-img" style={{ maxHeight: 150, position: 'absolute' }} />

                <div className="item-desc" style={{ marginLeft: 200 }}>
                    <span className="title">{item.title}</span>
                    <p>{item.desc}</p>
                    <p><b>Price: ${item.price}.00</b></p>
                    <p>
                        <b>Quantity: {item.quantity}</b>
                    </p>
                    <div className="add-remove">
                        <Link to="/cart" onClick={() => handlerAddQuantity(item.id)}><i className="material-icons">arrow_drop_up</i></Link>
                        <Link to="/cart" onClick={() => handlerSubtractQuantity(item.id)}><i className="material-icons">arrow_drop_down</i></Link>
                    </div>
                    <button className="waves-effect waves-light btn pink remove" onClick={() => handlerRemove(item.id)}>Remove</button>
                </div>
            </li>
        ));
    }

    return (
        <div className="container">
            <div className="cart">
                <h5>You have ordered:</h5>
                <ul className="collection">
                    {addedItems}
                </ul>
                <Recipe items={props.items}/>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        items: Object.values(state.addedItems),
        state: state,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeItem: (id) => { dispatch(removeItem(id)) },
        addQuantity: (id) => { dispatch(addQuantity(id)) },
        subtractQuantity: (id) => { dispatch(subtractQuantity(id)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)