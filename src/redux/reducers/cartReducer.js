import {
    ADD_TO_CART,
    REMOVE_ITEM,
    ADD_TO_QUANTITY,
    SUBTRACT_FROM_QUANTITY,
    ADD_SHIPPING,
    SUBTRACT_SHIPPING,
} from '../constants';

import Item1 from '../../images/item1.jpg'
import Item2 from '../../images/item2.jpg'
import Item3 from '../../images/item3.jpg'
import Item4 from '../../images/item4.jpg'
import Item5 from '../../images/item5.jpg'
import Item6 from '../../images/item6.jpg'


const initState = {
    items: {
        1: { id: 1, title: 'Winter body', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 110, img: Item1 },
        2: { id: 2, title: 'Adidas', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 80, img: Item2 },
        3: { id: 3, title: 'Vans', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 120, img: Item3 },
        4: { id: 4, title: 'White', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 260, img: Item4 },
        5: { id: 5, title: 'Cropped-sho', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 160, img: Item5 },
        6: { id: 6, title: 'Blues', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 90, img: Item6 }
    },

    addedItems: {},
    addShipping: false,
}

const cartReducer = (state = initState, action) => {
    if (action.type === ADD_TO_CART) {
        if (state.addedItems[action.id]) {
            const addedItems = { ...state.addedItems };
            addedItems[action.id].quantity++;

            return {
                ...state,
                addedItems,
            }
        }
        else {
            const item = {...state.items[action.id]};

            item.quantity = 1;

            return {
                ...state,
                addedItems: {
                    ...state.addedItems,
                    [action.id]: item,
                }
            }
        }
    }

    if (action.type === REMOVE_ITEM) {
        const addedItems = { ...state.addedItems };

        delete (addedItems[action.id]);

        return {
            ...state,
            addedItems,
        }
    }

    if (action.type === ADD_TO_QUANTITY) {
        const addedItems = { ...state.addedItems };
        addedItems[action.id].quantity++;

        return {
            ...state,
            addedItems,
        }
    }

    if (action.type === SUBTRACT_FROM_QUANTITY) {
        if (state.addedItems[action.id].quantity === 1) {
            const addedItems = { ...state.addedItems };

            delete (addedItems[action.id]);

            return {
                ...state,
                addedItems,
            }
        }
        else {
            const addedItems = { ...state.addedItems };
            addedItems[action.id].quantity--;

            return {
                ...state,
                addedItems,
            }
        }
    }

    if (action.type === ADD_SHIPPING) {
        return {
            ...state,
            addShipping: true,
        }
    }

    if (action.type === SUBTRACT_SHIPPING) {
        return {
            ...state,
            addShipping: false
        }
    }

    return state
}

export default cartReducer;